Hi.. thanks for looking at this project. 

So this rocket ODR (either pronounced Owther as in the mystical nordic force or O.D.R which can stand for Open Development Rocket) is an opensource design that can fly on 38mm motors and has been simulated on G through to I impulse motors. Therefore it could be used for mid power or high power rocketry and could be used to attain a level one high power certification. 

It is (as of 6th July 2018) as yet unflown but I am a moderately experienced rocket builder and have launched scratchbuilt airframes I have designed and simulated before. I will update when test flights have been achieved. 

It is designed and simulated in OpenRocket which is open source and my design file is included in this repo (odr.ork). The openrocket application is available from http://openrocket.info/ 

The simulation/design file is mostly complete and is currently set up with 100g of payload mass in the electronics bay which includes some of the weight of extra components such as the threaded bar (see ebay notes). Many mass values are overridden in the simulation with the real mass of the components I have made.. it's a good idea to weigh your own components and override these values with real data. 

There are many aspects of the design file that are representative of the design rather than actual cad of the components.. feel free to dial it in more and push back to here!

All the 3d print files are made in the opensource OpenScad http://www.openscad.org/

****** IMPORTANT ******* 

The gcode files for cutting the various boards are set up for a 1.5mm endmill at a feed rate of between 450 and 550 mm per minute and are formatted for a machine running GRBL

The STL files (you can remake them using the openscad files) are tweaked and sized for my printers and the current shrinkage etc of my filament.. you may need to tweak to your own calibration!


Finally.. the main tubes for this are available in the UK (and can be shipped elsewhere) from https://blackcatrocketry.co.uk
I have no connection to them but have found both their service excellent and their phenolic tubes are really good quality.

So.. thanks again.. feel free to get in touch via gitlab or twitter @concreted0g

If you build an ODR please let me know!


